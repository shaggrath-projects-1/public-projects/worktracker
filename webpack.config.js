const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const SRC_PATH = path.resolve(`${__dirname}/`, 'src');
const DIST_PATH = path.resolve(`${__dirname}/`, 'public');
const DEVELOPMENT = 'development';
const PRODUCTION = 'production';
const currentEnv = process.env.NODE_ENV || DEVELOPMENT;

module.exports = {
  name: 'workTracker',
  devtool: DEVELOPMENT === DEVELOPMENT ? 'source-map' : false,
  target: 'web',
  mode: currentEnv,
  entry: {
    index: [ path.resolve(SRC_PATH, 'index.tsx') ],
  },
  output: {
    filename: '[name].bundle.js',
    path: DIST_PATH,
    publicPath: '/',
  },
  optimization: {
    minimize: true,
    minimizer: [ new TerserPlugin() ],
  },
  resolve: {
    extensions: [ '.js', '.ts', '.tsx', '.json' ],
    modules: [
      'node_modules',
      SRC_PATH,
      '.',
    ],
    symlinks: false,
    cacheWithContext: false,
  },
  devServer: {
    compress: true,
    host: '0.0.0.0',
    port: 8181,
    https: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      filename: 'index.html',
      publicPath: '',
      template: path.resolve(SRC_PATH, 'index.pug'),
    }),
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.pug$/i,
        loader: 'pug-loader',
      },
      {
        test: /\.scss$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[local]----[hash:base64:3]',
              },
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.css$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
            },
          },
        ],
      },
    ],
  },
};

import React, { ReactNode } from 'react';
import type { FC, ChangeEvent } from 'react';
import type { ErrorListType } from 'types/Errors';
import type { ClosureFunctionType } from 'types/Functions';
import type { ValueType } from 'types/Fields';
import type { UseFormManagerOptions } from 'types/Global';

type ContextParamsType = {
  children?: ReactNode;
  errorMessages?: ErrorListType;
  fieldValueSelector?: {
    [ typeName: string ]: (...props: any) => ValueType;
  };
  customValidationRules?: {
    [ ruleName: string ]: ClosureFunctionType<void | string>
  };
  options?: UseFormManagerOptions;
};

const defaultValue: ContextParamsType = {
  errorMessages: {
    DEFAULT: 'Wrong format',
    REQUIRED: 'Current field is required!',
    CANNOT_BE_EMPTY: 'Current field is required!',
    NOT_DECIMAL: 'Wrong data type!',
    NOT_INTEGER: 'Wrong data type!',
    WRONG_EMAIL: 'Is invalid email!',
    FIELDS_NOT_EQUAL: 'Not equal!',
    TOO_LOW: 'Not valid value!',
    TOO_HIGH: 'Not valid value!',
    DATE_UNDER_MAX_RANGE: 'Date is under max range!',
    DATE_BELOW_MIN_RANGE: 'Date is below min range!',
  },
  fieldValueSelector: {
    checkbox: (event: ChangeEvent<HTMLInputElement>) => event.target.checked,
    default: (event: ChangeEvent<HTMLInputElement>) => event.target.value,
  },
  customValidationRules: {},
  options: {
    isValidateOnChange: false,
  },
};

export const ReactFormManagerContext = React.createContext<ContextParamsType>(defaultValue);

const ReactFormManagerProvider: FC<ContextParamsType> = (props) => {
  const {
    children,
    errorMessages,
    fieldValueSelector,
    customValidationRules,
    options,
  } = props;
  const data = {
    errorMessages: { ...defaultValue.errorMessages, ...errorMessages },
    customValidationRules: {
      ...defaultValue.customValidationRules || {},
      ...customValidationRules || {},
    },
    fieldValueSelector: {
      ...defaultValue.fieldValueSelector || {},
      ...fieldValueSelector || {},
    },
    options: {
      ...defaultValue.options,
      ...options,
    },
  };

  return (
    <ReactFormManagerContext.Provider value={data}>
      {children}
    </ReactFormManagerContext.Provider>
  );
};


export default ReactFormManagerProvider;

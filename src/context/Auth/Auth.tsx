import React, {
  createContext,
  useState,
  useRef,
  useEffect,
  FC,
  ReactNode, FormEvent,
} from 'react';
import cx from 'classnames';
import Cookies from 'js-cookie';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useSnackbar } from 'notistack';
import { App, Credentials, User } from 'realm-web';
import { useNavigate, useLocation } from 'react-router-dom';

import FormField from 'components/FormField';
import useReactFormManager from 'hooks/useReactFormManager';

import { signInFields } from './fields';
import styles from './Auth.scss';

type ProviderParamsType = {
  children?: ReactNode;
  appId: string;
};

type ContextDataType = {
  isSignIn: boolean;
  signIn: () => Promise<string | void>;
  signOut: () => void;
  token: string;
};

const TOKEN_STORE_KEY = 'token';

export const AuthContext = createContext<ContextDataType>({
  isSignIn: false,
  signIn: async () => undefined,
  signOut: () => undefined,
  token: '',
});

const AuthProvider: FC<ProviderParamsType> = (props) => {
  const {
    children,
    appId,
  } = props;
  const modalStyle = {
    maxHeight: 'calc(100vh - 200px)',
    overflow: 'auto',
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  const { validateForm, formFields } = useReactFormManager(signInFields);
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const location = useLocation();
  const signInResolveRef = useRef<(value: string | void | PromiseLike<string | void>) => void>(async () => {
  });
  const appRef = useRef(new App(appId));
  const [ token, setToken ] = useState<string>(Cookies.get(TOKEN_STORE_KEY) || '');
  const [ isOpenSignInForm, setIsOpenSignInForm ] = useState<boolean>(false);
  const [ isLoading, setIsLoading ] = useState<boolean>(false);

  const onSignInSubmitHandler = async (event: FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    setIsLoading(true);
    const validData = validateForm();
    if (!validData) {
      setIsLoading(false);
      return;
    }
    try {
      const credentials = Credentials.emailPassword(`${validData.login}@gmail.com`, validData.password);
      const user: User = await appRef.current.logIn(credentials);

      if (!user.accessToken) {
        throw new Error('!user.accessToken');
      }

      Cookies.set(TOKEN_STORE_KEY, user.accessToken, { expires: 7 });
      setToken(user.accessToken);
      onCloseSignInFormHandler(undefined, 'cb');
    } catch (exception) {
      enqueueSnackbar(<Typography sx={{ fontSize: 14 }}>Логін чи пароль невірні.</Typography>, {
        variant: 'error',
        anchorOrigin: {
          horizontal: 'center',
          vertical: 'top',
        },
      });
      signInResolveRef.current();
    } finally {
      setIsLoading(false);
    }
  };

  const onGoBackClickHandler = () => {
    navigate(-1);
  }

  const onCloseSignInFormHandler = (_: any, reason: string): void => {
    if (reason === 'backdropClick') {
      return;
    }

    setIsOpenSignInForm(false);
  };

  const signIn = (): Promise<string | void> => new Promise<string | void>(resolve => {
    signInResolveRef.current = resolve;
    setIsOpenSignInForm(true);
  });

  const signOut = () => {
    setToken('');
    Cookies.remove(TOKEN_STORE_KEY);
  };

  useEffect(() => {
    if (!!token) {
      signInResolveRef.current(token);
    }
  }, [token]);

  useEffect(() => {
    if (!location.pathname.includes('private')) {
      onCloseSignInFormHandler(undefined, 'locationChange');
    }
  }, [location.pathname]);

  const data: ContextDataType = {
    signIn,
    signOut,
    isSignIn: !!token,
    token,
  };

  return (
    <AuthContext.Provider value={data}>
      {children}
      <Modal
        open={isOpenSignInForm}
        onClose={onCloseSignInFormHandler}
      >
        <Box sx={modalStyle}>
          <Typography id="modal-modal-title" variant="h6" component="h2" sx={{ mb: 2 }}>
            {`Авторизація`}
          </Typography>
          <form
            className={cx(styles.Auth__formArea)}
            id="signIn"
            onSubmit={onSignInSubmitHandler}
          >
            {formFields.map(field => (<FormField key={field.name} {...field} disabled={isLoading}/>))}
            <Button
              type="submit"
              size="large"
              variant="contained"
              disabled={isLoading}
            >
              Авторизуватися
            </Button>
            <Button
              type="button"
              size="large"
              variant="text"
              disabled={isLoading}
              onClick={onGoBackClickHandler}
            >
              Повернутися назад
            </Button>
          </form>
        </Box>
      </Modal>
    </AuthContext.Provider>
  );
};


export default AuthProvider;

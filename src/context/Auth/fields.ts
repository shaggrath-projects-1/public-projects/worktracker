import type { FieldsType } from 'types/Fields';

export const signInFields: FieldsType = {
  login: {
    type: 'text',
    label: 'Логін',
    validationRules: [ 'string', 'required' ],
  },
  password: {
    type: 'password',
    label: 'Пароль',
    validationRules: [ 'string', 'required' ],
  },
};

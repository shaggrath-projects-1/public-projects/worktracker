type MathTypeType = 'round' | 'floor' | 'ceil';

export const decimalAdjust = (type: MathTypeType, value: number, exp: number): number => {
  // Если степень не определена, либо равна нулю...
  if (typeof exp === 'undefined' || +exp === 0) {
    return Math[type](value);
  }

  let newValue: string[] | number = +value;
  let newExp = +exp;

  // Если степень не определена, либо равна нулю...
  if (isNaN(newValue) || !(newExp % 1 === 0)) {
    return NaN;
  }

  // Сдвиг разрядов
  newValue = newValue.toString().split('e');
  newValue = Math[type](+(newValue[0] + 'e' + (newValue[1] ? (+newValue[1] - newExp) : -newExp)));
  // Обратный сдвиг
  newValue = newValue.toString().split('e');
  return +(newValue[0] + 'e' + (newValue[1] ? (+newValue[1] + newExp) : newExp));
}

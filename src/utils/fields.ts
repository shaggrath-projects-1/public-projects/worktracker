import type { ExtendedFieldType } from 'types/Fields';

export const getFieldType = (field: ExtendedFieldType, fields: ExtendedFieldType[]): string => {
  if (!field.showIf) {
    return  field.originalType;
  }

  const result = Object.keys(field.showIf).some(showIfKey => {
    const searchedField = fields.find(field => field.name === showIfKey);
    return searchedField && `${searchedField.value}` === `${(field.showIf || {})[showIfKey]}`
  })

  return !result ? 'hidden' : field.originalType;
}

import { gql } from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `DateTime` scalar type represents a DateTime. The DateTime is serialized as an RFC 3339 quoted string */
  DateTime: any;
  ObjectId: any;
};

export type Datum = {
  __typename?: 'Datum';
  _id?: Maybe<Scalars['ObjectId']>;
  altitude?: Maybe<Scalars['String']>;
  insideHumidity?: Maybe<Scalars['String']>;
  insidePressure?: Maybe<Scalars['String']>;
  insideTemperature?: Maybe<Scalars['String']>;
  outsideGas?: Maybe<Scalars['String']>;
  outsideHumidity?: Maybe<Scalars['String']>;
  outsidePressure?: Maybe<Scalars['String']>;
  outsideTemperature?: Maybe<Scalars['String']>;
  time?: Maybe<Scalars['DateTime']>;
};

export type DatumInsertInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  altitude?: InputMaybe<Scalars['String']>;
  insideHumidity?: InputMaybe<Scalars['String']>;
  insidePressure?: InputMaybe<Scalars['String']>;
  insideTemperature?: InputMaybe<Scalars['String']>;
  outsideGas?: InputMaybe<Scalars['String']>;
  outsideHumidity?: InputMaybe<Scalars['String']>;
  outsidePressure?: InputMaybe<Scalars['String']>;
  outsideTemperature?: InputMaybe<Scalars['String']>;
  time?: InputMaybe<Scalars['DateTime']>;
};

export type DatumQueryInput = {
  AND?: InputMaybe<Array<DatumQueryInput>>;
  OR?: InputMaybe<Array<DatumQueryInput>>;
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_exists?: InputMaybe<Scalars['Boolean']>;
  _id_gt?: InputMaybe<Scalars['ObjectId']>;
  _id_gte?: InputMaybe<Scalars['ObjectId']>;
  _id_in?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  _id_lt?: InputMaybe<Scalars['ObjectId']>;
  _id_lte?: InputMaybe<Scalars['ObjectId']>;
  _id_ne?: InputMaybe<Scalars['ObjectId']>;
  _id_nin?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  altitude?: InputMaybe<Scalars['String']>;
  altitude_exists?: InputMaybe<Scalars['Boolean']>;
  altitude_gt?: InputMaybe<Scalars['String']>;
  altitude_gte?: InputMaybe<Scalars['String']>;
  altitude_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  altitude_lt?: InputMaybe<Scalars['String']>;
  altitude_lte?: InputMaybe<Scalars['String']>;
  altitude_ne?: InputMaybe<Scalars['String']>;
  altitude_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  insideHumidity?: InputMaybe<Scalars['String']>;
  insideHumidity_exists?: InputMaybe<Scalars['Boolean']>;
  insideHumidity_gt?: InputMaybe<Scalars['String']>;
  insideHumidity_gte?: InputMaybe<Scalars['String']>;
  insideHumidity_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  insideHumidity_lt?: InputMaybe<Scalars['String']>;
  insideHumidity_lte?: InputMaybe<Scalars['String']>;
  insideHumidity_ne?: InputMaybe<Scalars['String']>;
  insideHumidity_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  insidePressure?: InputMaybe<Scalars['String']>;
  insidePressure_exists?: InputMaybe<Scalars['Boolean']>;
  insidePressure_gt?: InputMaybe<Scalars['String']>;
  insidePressure_gte?: InputMaybe<Scalars['String']>;
  insidePressure_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  insidePressure_lt?: InputMaybe<Scalars['String']>;
  insidePressure_lte?: InputMaybe<Scalars['String']>;
  insidePressure_ne?: InputMaybe<Scalars['String']>;
  insidePressure_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  insideTemperature?: InputMaybe<Scalars['String']>;
  insideTemperature_exists?: InputMaybe<Scalars['Boolean']>;
  insideTemperature_gt?: InputMaybe<Scalars['String']>;
  insideTemperature_gte?: InputMaybe<Scalars['String']>;
  insideTemperature_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  insideTemperature_lt?: InputMaybe<Scalars['String']>;
  insideTemperature_lte?: InputMaybe<Scalars['String']>;
  insideTemperature_ne?: InputMaybe<Scalars['String']>;
  insideTemperature_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsideGas?: InputMaybe<Scalars['String']>;
  outsideGas_exists?: InputMaybe<Scalars['Boolean']>;
  outsideGas_gt?: InputMaybe<Scalars['String']>;
  outsideGas_gte?: InputMaybe<Scalars['String']>;
  outsideGas_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsideGas_lt?: InputMaybe<Scalars['String']>;
  outsideGas_lte?: InputMaybe<Scalars['String']>;
  outsideGas_ne?: InputMaybe<Scalars['String']>;
  outsideGas_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsideHumidity?: InputMaybe<Scalars['String']>;
  outsideHumidity_exists?: InputMaybe<Scalars['Boolean']>;
  outsideHumidity_gt?: InputMaybe<Scalars['String']>;
  outsideHumidity_gte?: InputMaybe<Scalars['String']>;
  outsideHumidity_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsideHumidity_lt?: InputMaybe<Scalars['String']>;
  outsideHumidity_lte?: InputMaybe<Scalars['String']>;
  outsideHumidity_ne?: InputMaybe<Scalars['String']>;
  outsideHumidity_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsidePressure?: InputMaybe<Scalars['String']>;
  outsidePressure_exists?: InputMaybe<Scalars['Boolean']>;
  outsidePressure_gt?: InputMaybe<Scalars['String']>;
  outsidePressure_gte?: InputMaybe<Scalars['String']>;
  outsidePressure_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsidePressure_lt?: InputMaybe<Scalars['String']>;
  outsidePressure_lte?: InputMaybe<Scalars['String']>;
  outsidePressure_ne?: InputMaybe<Scalars['String']>;
  outsidePressure_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsideTemperature?: InputMaybe<Scalars['String']>;
  outsideTemperature_exists?: InputMaybe<Scalars['Boolean']>;
  outsideTemperature_gt?: InputMaybe<Scalars['String']>;
  outsideTemperature_gte?: InputMaybe<Scalars['String']>;
  outsideTemperature_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outsideTemperature_lt?: InputMaybe<Scalars['String']>;
  outsideTemperature_lte?: InputMaybe<Scalars['String']>;
  outsideTemperature_ne?: InputMaybe<Scalars['String']>;
  outsideTemperature_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  time?: InputMaybe<Scalars['DateTime']>;
  time_exists?: InputMaybe<Scalars['Boolean']>;
  time_gt?: InputMaybe<Scalars['DateTime']>;
  time_gte?: InputMaybe<Scalars['DateTime']>;
  time_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  time_lt?: InputMaybe<Scalars['DateTime']>;
  time_lte?: InputMaybe<Scalars['DateTime']>;
  time_ne?: InputMaybe<Scalars['DateTime']>;
  time_nin?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
};

export enum DatumSortByInput {
  AltitudeAsc = 'ALTITUDE_ASC',
  AltitudeDesc = 'ALTITUDE_DESC',
  InsidehumidityAsc = 'INSIDEHUMIDITY_ASC',
  InsidehumidityDesc = 'INSIDEHUMIDITY_DESC',
  InsidepressureAsc = 'INSIDEPRESSURE_ASC',
  InsidepressureDesc = 'INSIDEPRESSURE_DESC',
  InsidetemperatureAsc = 'INSIDETEMPERATURE_ASC',
  InsidetemperatureDesc = 'INSIDETEMPERATURE_DESC',
  OutsidegasAsc = 'OUTSIDEGAS_ASC',
  OutsidegasDesc = 'OUTSIDEGAS_DESC',
  OutsidehumidityAsc = 'OUTSIDEHUMIDITY_ASC',
  OutsidehumidityDesc = 'OUTSIDEHUMIDITY_DESC',
  OutsidepressureAsc = 'OUTSIDEPRESSURE_ASC',
  OutsidepressureDesc = 'OUTSIDEPRESSURE_DESC',
  OutsidetemperatureAsc = 'OUTSIDETEMPERATURE_ASC',
  OutsidetemperatureDesc = 'OUTSIDETEMPERATURE_DESC',
  TimeAsc = 'TIME_ASC',
  TimeDesc = 'TIME_DESC',
  IdAsc = '_ID_ASC',
  IdDesc = '_ID_DESC'
}

export type DatumUpdateInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_unset?: InputMaybe<Scalars['Boolean']>;
  altitude?: InputMaybe<Scalars['String']>;
  altitude_unset?: InputMaybe<Scalars['Boolean']>;
  insideHumidity?: InputMaybe<Scalars['String']>;
  insideHumidity_unset?: InputMaybe<Scalars['Boolean']>;
  insidePressure?: InputMaybe<Scalars['String']>;
  insidePressure_unset?: InputMaybe<Scalars['Boolean']>;
  insideTemperature?: InputMaybe<Scalars['String']>;
  insideTemperature_unset?: InputMaybe<Scalars['Boolean']>;
  outsideGas?: InputMaybe<Scalars['String']>;
  outsideGas_unset?: InputMaybe<Scalars['Boolean']>;
  outsideHumidity?: InputMaybe<Scalars['String']>;
  outsideHumidity_unset?: InputMaybe<Scalars['Boolean']>;
  outsidePressure?: InputMaybe<Scalars['String']>;
  outsidePressure_unset?: InputMaybe<Scalars['Boolean']>;
  outsideTemperature?: InputMaybe<Scalars['String']>;
  outsideTemperature_unset?: InputMaybe<Scalars['Boolean']>;
  time?: InputMaybe<Scalars['DateTime']>;
  time_unset?: InputMaybe<Scalars['Boolean']>;
};

export type DeleteManyPayload = {
  __typename?: 'DeleteManyPayload';
  deletedCount: Scalars['Int'];
};

export type InsertManyPayload = {
  __typename?: 'InsertManyPayload';
  insertedIds: Array<Maybe<Scalars['ObjectId']>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  deleteManyData?: Maybe<DeleteManyPayload>;
  deleteOneDatum?: Maybe<Datum>;
  insertManyData?: Maybe<InsertManyPayload>;
  insertOneDatum?: Maybe<Datum>;
  replaceOneDatum?: Maybe<Datum>;
  updateManyData?: Maybe<UpdateManyPayload>;
  updateOneDatum?: Maybe<Datum>;
  upsertOneDatum?: Maybe<Datum>;
};


export type MutationDeleteManyDataArgs = {
  query?: InputMaybe<DatumQueryInput>;
};


export type MutationDeleteOneDatumArgs = {
  query: DatumQueryInput;
};


export type MutationInsertManyDataArgs = {
  data: Array<DatumInsertInput>;
};


export type MutationInsertOneDatumArgs = {
  data: DatumInsertInput;
};


export type MutationReplaceOneDatumArgs = {
  data: DatumInsertInput;
  query?: InputMaybe<DatumQueryInput>;
};


export type MutationUpdateManyDataArgs = {
  query?: InputMaybe<DatumQueryInput>;
  set: DatumUpdateInput;
};


export type MutationUpdateOneDatumArgs = {
  query?: InputMaybe<DatumQueryInput>;
  set: DatumUpdateInput;
};


export type MutationUpsertOneDatumArgs = {
  data: DatumInsertInput;
  query?: InputMaybe<DatumQueryInput>;
};

export type Query = {
  __typename?: 'Query';
  data: Array<Maybe<Datum>>;
  datum?: Maybe<Datum>;
};


export type QueryDataArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  query?: InputMaybe<DatumQueryInput>;
  sortBy?: InputMaybe<DatumSortByInput>;
};


export type QueryDatumArgs = {
  query?: InputMaybe<DatumQueryInput>;
};

export type UpdateManyPayload = {
  __typename?: 'UpdateManyPayload';
  matchedCount: Scalars['Int'];
  modifiedCount: Scalars['Int'];
};

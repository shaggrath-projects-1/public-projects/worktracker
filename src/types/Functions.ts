export type ClosureFunctionType<T = void> = () => () => T;

export type ErrorListType = {
  [errorKey: string]: string;
};

export type SetErrorType = (message: string) => void;

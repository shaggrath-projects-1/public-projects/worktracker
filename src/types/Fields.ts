import type { ErrorListType, SetErrorType } from 'types/Errors';

// export type ValueType = string | number | boolean | ValueType[];
export type ValueType = any;

export type SetValueType = (value: ValueType) => void;

export type ValidationRulesType = any;

export type FieldOptionType = {
  label: string | number,
  value: string | number,
  image?: string,
  description?: string,
}

export type FieldType = {
  label: string;
  type: string;
  validationRules: ValidationRulesType;
  defaultValue?: ValueType;
  options?: FieldOptionType[];
  showIf?: { [ fieldName: string ]: ValueType };
  htmlAttr?: any;
  errorMessages?: ErrorListType;
  onBeforeChange?: (value: ValueType) => boolean;
  onAfterChange?: (value: ValueType) => void;
};

export type ExtendedFieldType = FieldType & {
  originalType: string;
  name: string;
  value?: ValueType;
  onBeforeChange: (value: ValueType, bulkUpdateFields?: BulkUpdateFieldsType) => boolean;
  onAfterChange: (value: ValueType, bulkUpdateFields?: BulkUpdateFieldsType) => void;
}

export type FieldObjectType = {
  name: string;
  value: ValueType;
  field: ExtendedFieldType
  isError: boolean;
  errorMessage: string;
  bind: {
    name: string;
    id: string;
    type: string;
    value: ValueType;
    onChange: () => void;
    [ htmlAttr: string ]: any;
  };
  setValue: SetValueType;
  setType: (type: string) => void;
  setIsError: (errorStatus: boolean) => void;
  setErrorMessage: (errorMessage: string) => void;
};

export type ValidatorDataType = {
  rules: { [fieldName: string]: ValidationRulesType },
  data: { [fieldName: string]: ValueType },
}

export type ValidationResult = {
  [fieldName: string]: ValueType;
} | void;

export type FieldsType = {
  [ fieldName: string ]: FieldType;
};

export type FieldsObjectType = {
  [name: string]: {
    value: ValueType;
    setValue: SetValueType;
    setError: SetErrorType;
  };
}

export type BulkUpdateFieldsType = (fields: {[name: string]: ValueType}) => void;

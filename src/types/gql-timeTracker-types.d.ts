import { gql } from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `DateTime` scalar type represents a DateTime. The DateTime is serialized as an RFC 3339 quoted string */
  DateTime: any;
  ObjectId: any;
};

export type DeleteManyPayload = {
  __typename?: 'DeleteManyPayload';
  deletedCount: Scalars['Int'];
};

export type InsertManyPayload = {
  __typename?: 'InsertManyPayload';
  insertedIds: Array<Maybe<Scalars['ObjectId']>>;
};

export type Log = {
  __typename?: 'Log';
  _id?: Maybe<Scalars['ObjectId']>;
  date?: Maybe<Scalars['DateTime']>;
  desc?: Maybe<Scalars['String']>;
  timeEnd?: Maybe<Scalars['DateTime']>;
  timeStart?: Maybe<Scalars['DateTime']>;
  user?: Maybe<User>;
};

export type LogInsertInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  date?: InputMaybe<Scalars['DateTime']>;
  desc?: InputMaybe<Scalars['String']>;
  timeEnd?: InputMaybe<Scalars['DateTime']>;
  timeStart?: InputMaybe<Scalars['DateTime']>;
  user?: InputMaybe<LogUserRelationInput>;
};

export type LogQueryInput = {
  AND?: InputMaybe<Array<LogQueryInput>>;
  OR?: InputMaybe<Array<LogQueryInput>>;
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_exists?: InputMaybe<Scalars['Boolean']>;
  _id_gt?: InputMaybe<Scalars['ObjectId']>;
  _id_gte?: InputMaybe<Scalars['ObjectId']>;
  _id_in?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  _id_lt?: InputMaybe<Scalars['ObjectId']>;
  _id_lte?: InputMaybe<Scalars['ObjectId']>;
  _id_ne?: InputMaybe<Scalars['ObjectId']>;
  _id_nin?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  date?: InputMaybe<Scalars['DateTime']>;
  date_exists?: InputMaybe<Scalars['Boolean']>;
  date_gt?: InputMaybe<Scalars['DateTime']>;
  date_gte?: InputMaybe<Scalars['DateTime']>;
  date_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  date_lt?: InputMaybe<Scalars['DateTime']>;
  date_lte?: InputMaybe<Scalars['DateTime']>;
  date_ne?: InputMaybe<Scalars['DateTime']>;
  date_nin?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  desc?: InputMaybe<Scalars['String']>;
  desc_exists?: InputMaybe<Scalars['Boolean']>;
  desc_gt?: InputMaybe<Scalars['String']>;
  desc_gte?: InputMaybe<Scalars['String']>;
  desc_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  desc_lt?: InputMaybe<Scalars['String']>;
  desc_lte?: InputMaybe<Scalars['String']>;
  desc_ne?: InputMaybe<Scalars['String']>;
  desc_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  timeEnd?: InputMaybe<Scalars['DateTime']>;
  timeEnd_exists?: InputMaybe<Scalars['Boolean']>;
  timeEnd_gt?: InputMaybe<Scalars['DateTime']>;
  timeEnd_gte?: InputMaybe<Scalars['DateTime']>;
  timeEnd_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  timeEnd_lt?: InputMaybe<Scalars['DateTime']>;
  timeEnd_lte?: InputMaybe<Scalars['DateTime']>;
  timeEnd_ne?: InputMaybe<Scalars['DateTime']>;
  timeEnd_nin?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  timeStart?: InputMaybe<Scalars['DateTime']>;
  timeStart_exists?: InputMaybe<Scalars['Boolean']>;
  timeStart_gt?: InputMaybe<Scalars['DateTime']>;
  timeStart_gte?: InputMaybe<Scalars['DateTime']>;
  timeStart_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  timeStart_lt?: InputMaybe<Scalars['DateTime']>;
  timeStart_lte?: InputMaybe<Scalars['DateTime']>;
  timeStart_ne?: InputMaybe<Scalars['DateTime']>;
  timeStart_nin?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  user?: InputMaybe<UserQueryInput>;
  user_exists?: InputMaybe<Scalars['Boolean']>;
};

export enum LogSortByInput {
  DateAsc = 'DATE_ASC',
  DateDesc = 'DATE_DESC',
  DescAsc = 'DESC_ASC',
  DescDesc = 'DESC_DESC',
  TimeendAsc = 'TIMEEND_ASC',
  TimeendDesc = 'TIMEEND_DESC',
  TimestartAsc = 'TIMESTART_ASC',
  TimestartDesc = 'TIMESTART_DESC',
  UserAsc = 'USER_ASC',
  UserDesc = 'USER_DESC',
  IdAsc = '_ID_ASC',
  IdDesc = '_ID_DESC'
}

export type LogUpdateInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_unset?: InputMaybe<Scalars['Boolean']>;
  date?: InputMaybe<Scalars['DateTime']>;
  date_unset?: InputMaybe<Scalars['Boolean']>;
  desc?: InputMaybe<Scalars['String']>;
  desc_unset?: InputMaybe<Scalars['Boolean']>;
  timeEnd?: InputMaybe<Scalars['DateTime']>;
  timeEnd_unset?: InputMaybe<Scalars['Boolean']>;
  timeStart?: InputMaybe<Scalars['DateTime']>;
  timeStart_unset?: InputMaybe<Scalars['Boolean']>;
  user?: InputMaybe<LogUserRelationInput>;
  user_unset?: InputMaybe<Scalars['Boolean']>;
};

export type LogUserRelationInput = {
  create?: InputMaybe<UserInsertInput>;
  link?: InputMaybe<Scalars['ObjectId']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  deleteManyLogs?: Maybe<DeleteManyPayload>;
  deleteManyShifts?: Maybe<DeleteManyPayload>;
  deleteManyUsers?: Maybe<DeleteManyPayload>;
  deleteOneLog?: Maybe<Log>;
  deleteOneShift?: Maybe<Shift>;
  deleteOneUser?: Maybe<User>;
  insertManyLogs?: Maybe<InsertManyPayload>;
  insertManyShifts?: Maybe<InsertManyPayload>;
  insertManyUsers?: Maybe<InsertManyPayload>;
  insertOneLog?: Maybe<Log>;
  insertOneShift?: Maybe<Shift>;
  insertOneUser?: Maybe<User>;
  replaceOneLog?: Maybe<Log>;
  replaceOneShift?: Maybe<Shift>;
  replaceOneUser?: Maybe<User>;
  updateManyLogs?: Maybe<UpdateManyPayload>;
  updateManyShifts?: Maybe<UpdateManyPayload>;
  updateManyUsers?: Maybe<UpdateManyPayload>;
  updateOneLog?: Maybe<Log>;
  updateOneShift?: Maybe<Shift>;
  updateOneUser?: Maybe<User>;
  upsertOneLog?: Maybe<Log>;
  upsertOneShift?: Maybe<Shift>;
  upsertOneUser?: Maybe<User>;
};


export type MutationDeleteManyLogsArgs = {
  query?: InputMaybe<LogQueryInput>;
};


export type MutationDeleteManyShiftsArgs = {
  query?: InputMaybe<ShiftQueryInput>;
};


export type MutationDeleteManyUsersArgs = {
  query?: InputMaybe<UserQueryInput>;
};


export type MutationDeleteOneLogArgs = {
  query: LogQueryInput;
};


export type MutationDeleteOneShiftArgs = {
  query: ShiftQueryInput;
};


export type MutationDeleteOneUserArgs = {
  query: UserQueryInput;
};


export type MutationInsertManyLogsArgs = {
  data: Array<LogInsertInput>;
};


export type MutationInsertManyShiftsArgs = {
  data: Array<ShiftInsertInput>;
};


export type MutationInsertManyUsersArgs = {
  data: Array<UserInsertInput>;
};


export type MutationInsertOneLogArgs = {
  data: LogInsertInput;
};


export type MutationInsertOneShiftArgs = {
  data: ShiftInsertInput;
};


export type MutationInsertOneUserArgs = {
  data: UserInsertInput;
};


export type MutationReplaceOneLogArgs = {
  data: LogInsertInput;
  query?: InputMaybe<LogQueryInput>;
};


export type MutationReplaceOneShiftArgs = {
  data: ShiftInsertInput;
  query?: InputMaybe<ShiftQueryInput>;
};


export type MutationReplaceOneUserArgs = {
  data: UserInsertInput;
  query?: InputMaybe<UserQueryInput>;
};


export type MutationUpdateManyLogsArgs = {
  query?: InputMaybe<LogQueryInput>;
  set: LogUpdateInput;
};


export type MutationUpdateManyShiftsArgs = {
  query?: InputMaybe<ShiftQueryInput>;
  set: ShiftUpdateInput;
};


export type MutationUpdateManyUsersArgs = {
  query?: InputMaybe<UserQueryInput>;
  set: UserUpdateInput;
};


export type MutationUpdateOneLogArgs = {
  query?: InputMaybe<LogQueryInput>;
  set: LogUpdateInput;
};


export type MutationUpdateOneShiftArgs = {
  query?: InputMaybe<ShiftQueryInput>;
  set: ShiftUpdateInput;
};


export type MutationUpdateOneUserArgs = {
  query?: InputMaybe<UserQueryInput>;
  set: UserUpdateInput;
};


export type MutationUpsertOneLogArgs = {
  data: LogInsertInput;
  query?: InputMaybe<LogQueryInput>;
};


export type MutationUpsertOneShiftArgs = {
  data: ShiftInsertInput;
  query?: InputMaybe<ShiftQueryInput>;
};


export type MutationUpsertOneUserArgs = {
  data: UserInsertInput;
  query?: InputMaybe<UserQueryInput>;
};

export type Query = {
  __typename?: 'Query';
  log?: Maybe<Log>;
  logs: Array<Maybe<Log>>;
  shift?: Maybe<Shift>;
  shifts: Array<Maybe<Shift>>;
  user?: Maybe<User>;
  users: Array<Maybe<User>>;
};


export type QueryLogArgs = {
  query?: InputMaybe<LogQueryInput>;
};


export type QueryLogsArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  query?: InputMaybe<LogQueryInput>;
  sortBy?: InputMaybe<LogSortByInput>;
};


export type QueryShiftArgs = {
  query?: InputMaybe<ShiftQueryInput>;
};


export type QueryShiftsArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  query?: InputMaybe<ShiftQueryInput>;
  sortBy?: InputMaybe<ShiftSortByInput>;
};


export type QueryUserArgs = {
  query?: InputMaybe<UserQueryInput>;
};


export type QueryUsersArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  query?: InputMaybe<UserQueryInput>;
  sortBy?: InputMaybe<UserSortByInput>;
};

export type Shift = {
  __typename?: 'Shift';
  _id?: Maybe<Scalars['ObjectId']>;
  date?: Maybe<Scalars['DateTime']>;
  uuid?: Maybe<Scalars['String']>;
};

export type ShiftInsertInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  date?: InputMaybe<Scalars['DateTime']>;
  uuid?: InputMaybe<Scalars['String']>;
};

export type ShiftQueryInput = {
  AND?: InputMaybe<Array<ShiftQueryInput>>;
  OR?: InputMaybe<Array<ShiftQueryInput>>;
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_exists?: InputMaybe<Scalars['Boolean']>;
  _id_gt?: InputMaybe<Scalars['ObjectId']>;
  _id_gte?: InputMaybe<Scalars['ObjectId']>;
  _id_in?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  _id_lt?: InputMaybe<Scalars['ObjectId']>;
  _id_lte?: InputMaybe<Scalars['ObjectId']>;
  _id_ne?: InputMaybe<Scalars['ObjectId']>;
  _id_nin?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  date?: InputMaybe<Scalars['DateTime']>;
  date_exists?: InputMaybe<Scalars['Boolean']>;
  date_gt?: InputMaybe<Scalars['DateTime']>;
  date_gte?: InputMaybe<Scalars['DateTime']>;
  date_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  date_lt?: InputMaybe<Scalars['DateTime']>;
  date_lte?: InputMaybe<Scalars['DateTime']>;
  date_ne?: InputMaybe<Scalars['DateTime']>;
  date_nin?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
  uuid?: InputMaybe<Scalars['String']>;
  uuid_exists?: InputMaybe<Scalars['Boolean']>;
  uuid_gt?: InputMaybe<Scalars['String']>;
  uuid_gte?: InputMaybe<Scalars['String']>;
  uuid_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  uuid_lt?: InputMaybe<Scalars['String']>;
  uuid_lte?: InputMaybe<Scalars['String']>;
  uuid_ne?: InputMaybe<Scalars['String']>;
  uuid_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export enum ShiftSortByInput {
  DateAsc = 'DATE_ASC',
  DateDesc = 'DATE_DESC',
  UuidAsc = 'UUID_ASC',
  UuidDesc = 'UUID_DESC',
  IdAsc = '_ID_ASC',
  IdDesc = '_ID_DESC'
}

export type ShiftUpdateInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_unset?: InputMaybe<Scalars['Boolean']>;
  date?: InputMaybe<Scalars['DateTime']>;
  date_unset?: InputMaybe<Scalars['Boolean']>;
  uuid?: InputMaybe<Scalars['String']>;
  uuid_unset?: InputMaybe<Scalars['Boolean']>;
};

export type UpdateManyPayload = {
  __typename?: 'UpdateManyPayload';
  matchedCount: Scalars['Int'];
  modifiedCount: Scalars['Int'];
};

export type User = {
  __typename?: 'User';
  _id?: Maybe<Scalars['ObjectId']>;
  name?: Maybe<Scalars['String']>;
  uuid?: Maybe<Scalars['String']>;
};

export type UserInsertInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  name?: InputMaybe<Scalars['String']>;
  uuid?: InputMaybe<Scalars['String']>;
};

export type UserQueryInput = {
  AND?: InputMaybe<Array<UserQueryInput>>;
  OR?: InputMaybe<Array<UserQueryInput>>;
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_exists?: InputMaybe<Scalars['Boolean']>;
  _id_gt?: InputMaybe<Scalars['ObjectId']>;
  _id_gte?: InputMaybe<Scalars['ObjectId']>;
  _id_in?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  _id_lt?: InputMaybe<Scalars['ObjectId']>;
  _id_lte?: InputMaybe<Scalars['ObjectId']>;
  _id_ne?: InputMaybe<Scalars['ObjectId']>;
  _id_nin?: InputMaybe<Array<InputMaybe<Scalars['ObjectId']>>>;
  name?: InputMaybe<Scalars['String']>;
  name_exists?: InputMaybe<Scalars['Boolean']>;
  name_gt?: InputMaybe<Scalars['String']>;
  name_gte?: InputMaybe<Scalars['String']>;
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  name_lt?: InputMaybe<Scalars['String']>;
  name_lte?: InputMaybe<Scalars['String']>;
  name_ne?: InputMaybe<Scalars['String']>;
  name_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  uuid?: InputMaybe<Scalars['String']>;
  uuid_exists?: InputMaybe<Scalars['Boolean']>;
  uuid_gt?: InputMaybe<Scalars['String']>;
  uuid_gte?: InputMaybe<Scalars['String']>;
  uuid_in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  uuid_lt?: InputMaybe<Scalars['String']>;
  uuid_lte?: InputMaybe<Scalars['String']>;
  uuid_ne?: InputMaybe<Scalars['String']>;
  uuid_nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export enum UserSortByInput {
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  UuidAsc = 'UUID_ASC',
  UuidDesc = 'UUID_DESC',
  IdAsc = '_ID_ASC',
  IdDesc = '_ID_DESC'
}

export type UserUpdateInput = {
  _id?: InputMaybe<Scalars['ObjectId']>;
  _id_unset?: InputMaybe<Scalars['Boolean']>;
  name?: InputMaybe<Scalars['String']>;
  name_unset?: InputMaybe<Scalars['Boolean']>;
  uuid?: InputMaybe<Scalars['String']>;
  uuid_unset?: InputMaybe<Scalars['Boolean']>;
};

import React, { FC, ReactElement, ReactNode } from 'react';
import cx from 'classnames';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Add from '@mui/icons-material/Add';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import styles from './DrawerLayout.scss';

type DrawerLayoutPropsType = {
  asideNode: ReactNode,
  mainNode: ReactNode,
}

const DrawerLayout: FC<DrawerLayoutPropsType> = (props): ReactElement => {
  const {
    asideNode,
    mainNode,
  } = props;

  const [ open, setOpen ] = React.useState(true);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box className={cx(styles.DrawerLayout__container)}>
      <div
        className={cx(styles.DrawerLayout__btnWrapper, {
          [ styles[ 'DrawerLayout--isOpen' ] ]: open,
          [ styles[ 'DrawerLayout--isClosed' ] ]: !open,
        })}
      >
        <IconButton
          onClick={handleDrawerOpen}
          className={cx(styles.DrawerLayout__btn, styles[ 'DrawerLayout__btn--burger' ])}
        >
          <Add/>
        </IconButton>
        <IconButton
          onClick={handleDrawerClose}
          className={cx(styles.DrawerLayout__btn, styles[ 'DrawerLayout__btn--arrow' ])}
        >
          <ChevronLeftIcon/>
        </IconButton>
      </div>
      <div className={cx(styles.DrawerLayout)}>
        <div
          className={cx(
            { [ styles[ 'DrawerLayout--isOpen' ] ]: open },
            { [ styles[ 'DrawerLayout--isClosed' ] ]: !open },
          )}
        >
          {asideNode}
        </div>
        <div className={cx(styles.DrawerLayout__divider)}/>
        <Box
          component="div"
          className={cx(styles.DrawerLayout__main)}
        >
          {mainNode}
        </Box>
      </div>
    </Box>
  );
};

export default DrawerLayout;

import React, { FC, ReactElement, ReactNode, useRef, useState } from 'react';
import cx from 'classnames';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { NavLink } from 'react-router-dom';
import useAuth from 'hooks/useAuth';
import MenuIcon from '@mui/icons-material/Menu';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import GroupIcon from '@mui/icons-material/Group';
import styles from './MainLayout.scss';

type MainLayoutPropsType = {
  children: ReactNode,
}

const MainLayout: FC<MainLayoutPropsType> = (props): ReactElement => {
  const {
    children,
  } = props;
  const menuButtonRef = useRef<any>();
  const [ isOpenMenu, setIsOpenMenu ] = useState(false);
  const { signIn, signOut, isSignIn } = useAuth();
  const toggleMenuHandler = () => {
    setIsOpenMenu(prev => !prev);
  };
  const onCloseMenuHandler = () => setIsOpenMenu(false);
  const nav = [
    { link: '/', label: 'Погода', IconComponent: WbSunnyIcon },
    { link: '/private/work-tracker/', label: 'Звіт по роботі', IconComponent: GroupIcon },
    { link: '/private/shifts/', label: 'Звіт по караулам', IconComponent: GroupIcon },
  ];
  return (
    <Box className={cx(styles.MainLayout)}>
      <AppBar
        component="nav"
        classes={{
          root: cx(styles.MainLayout__appBar)
        }}
      >
        <div ref={menuButtonRef}>
          <Button
            onClick={toggleMenuHandler}
            ref={menuButtonRef.current}
            classes={{
              root: cx(styles.MainLayout__menuBtn)
            }}
          >
            <MenuIcon
              classes={{
                root: cx(styles.MainLayout__menuIcon)
              }}
            />
          </Button>
        </div>
        <Menu
          id="basic-menu"
          anchorEl={menuButtonRef.current}
          open={isOpenMenu}
          onClose={onCloseMenuHandler}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
          classes={{
            paper: cx(styles.MainLayout__paper)
          }}
        >
          {nav.map(item => (
            <MenuItem
              onClick={onCloseMenuHandler}
              key={item.link}
              classes={{
                root: cx(styles.MainLayout__menuItem)
              }}
            >

              <NavLink
                end
                className={({ isActive }) => cx(styles.MainLayout__navItem, { [ styles[ 'MainLayout__navItem--active' ] ]: isActive })}
                to={item.link}
              >
                {({ isActive }) => (
                  <>
                    <item.IconComponent className={cx(styles.MainLayout__navLink)} />
                    <Button
                      disabled={isActive}
                      classes={{
                        root: cx(styles.MainLayout__navLink),
                      }}
                    >
                      {item.label}
                    </Button>
                  </>
                )}
              </NavLink>
            </MenuItem>
          ))}
        </Menu>
        <Button
          size="large"
          variant="contained"
          onClick={isSignIn ? signOut : signIn}
        >{isSignIn ? 'Вийти' : 'Авторизуватись'}</Button>
      </AppBar>
      <Box className={cx(styles.MainLayout__content)} component="main">
        <Toolbar />
        {children}
      </Box>
    </Box>
  );
};

export default MainLayout;

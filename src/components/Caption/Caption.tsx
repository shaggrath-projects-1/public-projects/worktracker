import React from 'react';
import type {FC, ReactElement, ReactNode} from 'react';
import cx from 'classnames';
import Typography from '@mui/material/Typography';
import styles from './Caption.scss';

type LabelPropsType = {
  children: ReactNode;
}

const Caption: FC<LabelPropsType> = (props): ReactElement => {
  const { children } = props;
  return (
    <Typography className={cx(styles.Caption)}>{children}</Typography>
  )
};

export default Caption;

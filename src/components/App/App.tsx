import type { FC, ReactElement } from 'react';
import React from 'react';
import config from 'config';
import { HashRouter } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import locale from 'dayjs/locale/uk';
import eUkraine from 'styles/fonts/e-UkraineHead-Regular.woff2';
import { green, orange, deepOrange, blue } from '@mui/material/colors';
// LocalizationProvider

import Root from 'routes/Root';
import AuthProvider from 'context/Auth';
import ReactFormManagerProvider from 'context/ReactFormManager';

const App: FC = (): ReactElement => {
  const theme = createTheme({
    palette: {
      mode: 'dark',
      primary: {
        light: '#0059B2',
        main: '#007FFF',
        dark: '#0059B2',
        contrastText: '#fff',
      },
      background: {
        default: '#0A1929',
        paper: '#001E3C',
      },
      secondary: {
        main: '#0059B2',
      },
      //   divider: '#0A1929',
    },
    typography: {
      htmlFontSize: 10,
      fontFamily: 'eUkraine, Arial',
    },
    components: {
      MuiCssBaseline: {
        styleOverrides: `
        @font-face {
          font-family: 'eUkraine';
          font-style: normal;
          font-display: swap;
          font-weight: 400;
          src: local('eUkraine'), local('eUkraine-Regular'), url(${eUkraine}) format('woff2');
        }
      `,
      },
    },
  });
  dayjs.locale('uk', locale);
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline/>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <ReactFormManagerProvider
            fieldValueSelector={{
              autocomplete: (event, newVal) => {
                return newVal?.value || '';
              },
              autocompleteMultiple: (event, newVal) => {
                return newVal.map((item: any) => item.value) || [];
              },
              time: (newVal) => {
                return newVal.toDate();
              },
              date: (newVal) => {
                return newVal.toDate();
              },
            }}
          >
            <SnackbarProvider maxSnack={5}>
              <HashRouter basename={'/'}>
                <AuthProvider appId={config.realm.workTracker.appId}>
                  <Root/>
                </AuthProvider>
              </HashRouter>
            </SnackbarProvider>
          </ReactFormManagerProvider>
        </LocalizationProvider>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default App;

import React, { forwardRef } from 'react';
import type { ReactElement, ReactNode } from 'react';
import cx from 'classnames';
import styles from './Box.scss';

type BoxPropsType = {
  className?: string;
  viewType?: 'primary' | 'dark' | 'secondary';
  children: ReactNode;
}

const Box = forwardRef<HTMLDivElement, BoxPropsType>((props, ref): ReactElement => {
  const {
    className,
    viewType = 'primary',
    children,
  } = props;

  return (
    <div
      ref={ref}
      className={cx(
        styles.Box,
        { [ styles[ 'Box--dark' ] ]: viewType === 'dark' },
        { [ styles[ 'Box--secondary' ] ]: viewType === 'secondary' },
        className,
      )}
    >
      {children}
    </div>
  );
});

export default Box;

import React, { FC, ReactElement } from 'react';
import CircularProgress from '@mui/material/CircularProgress';

const Loader: FC = (): ReactElement => {
  return (
    <>
      <CircularProgress />
    </>
  )
};

export default Loader;

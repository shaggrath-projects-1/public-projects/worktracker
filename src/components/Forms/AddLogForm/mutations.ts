import { gql } from '@apollo/client';

export const mutationAddLog = gql`
  mutation ($data: LogInsertInput!) {
      insertOneLog(data: $data) {
          _id
      }
  }
`;

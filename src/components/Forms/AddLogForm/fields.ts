import type { FieldsType } from 'types/Fields';

export const addToLogFields: FieldsType = {
  users: {
    type: 'autocompleteMultiple',
    label: 'Солдат',
    validationRules: [ 'required', { list_of: [ 'string' ] } ],
    defaultValue: [],
  },
  date: {
    type: 'date',
    label: 'Дата',
    validationRules: [ 'required' ],
    defaultValue: new Date(),
  },
  timeStart: {
    type: 'time',
    label: 'Час початку роботи',
    validationRules: [ 'required' ],
    defaultValue: new Date(),
  },
  timeEnd: {
    type: 'time',
    label: 'Час закінчення роботи',
    validationRules: [ 'required' ],
    defaultValue: new Date(),
  },
  desc: {
    type: 'textarea',
    label: 'Опис роботи',
    validationRules: [ 'string', 'required' ],
  },
};

import React, { FormEvent } from 'react';
import type { FC, ReactElement } from 'react';
import type { FieldOptionType } from 'types/Fields';
import cx from 'classnames';
import { useMutation } from '@apollo/client';
import Box from 'components/Box';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';
import { useSnackbar } from 'notistack';

import useReactFormManager from 'hooks/useReactFormManager';
import FormField from 'components/FormField';
import { addToLogFields } from './fields';
import { mutationAddLog } from './mutations';
import styles from './AddLogForm.scss';
import Typography from '@mui/material/Typography';

type AddLogFormPropsType = {
  classNames?: string;
  onSubmitSuccess?: () => void;
  userOptions: FieldOptionType[];
}

const AddLogForm: FC<AddLogFormPropsType> = (props): ReactElement => {
  const {
    userOptions,
    classNames,
    onSubmitSuccess = () => undefined,
  } = props;
  addToLogFields.users.options = userOptions;
  const { enqueueSnackbar } = useSnackbar();
  const { formFields, validateForm, resetForm } = useReactFormManager(addToLogFields);
  const [ addLog, { loading } ] = useMutation(mutationAddLog);

  const onSubmitHandler = async (event: FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    const validData = validateForm();
    if (!validData) {
      return;
    }

    try {
      await Promise.all(validData.users.map((user: string) => addLog({
        variables: {
          data: {
            ...validData,
            user: { link: user },
            users: undefined,
          },
        },
      })));
      onSubmitSuccess();
    } catch (exception) {
      return;
    }

    enqueueSnackbar(<Typography sx={{ fontSize: 14 }}>Данні збережені.</Typography>, {
      variant: 'success',
      anchorOrigin: {
        horizontal: 'center',
        vertical: 'top',
      },
    });
    resetForm();
  };

  return (
    <Box viewType="secondary" className={cx(styles.AddLogForm__box)}>
      <CardHeader
        title="Додати запис про виконану роботу"
        classes={{
          content: cx(styles.AddLogForm__content),
          title: cx(styles.AddLogForm__title)
        }}
      />
      <CardContent>
        <form id="save-data" onSubmit={onSubmitHandler} className={cx(classNames, styles.AddLogForm)}>
          {formFields.map(formField => (
            <FormField key={formField.name} {...formField} />
          ))}
        </form>
      </CardContent>
      <CardActions
        classes={{
          root: cx(styles.AddLogForm__actions)
        }}
      >
        <Button
          disabled={loading}
          form="save-data"
          size="large"
          variant="contained"
          type="submit"
        >
          Додати
        </Button>
      </CardActions>
    </Box>
  );
};

export default AddLogForm;

import React, { useRef, useState } from 'react';
import type { FC, ReactElement } from 'react';
import dayjs, { Dayjs } from 'dayjs';
import cx from 'classnames';

import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { MobileTimePicker } from '@mui/x-date-pickers/MobileTimePicker';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';

import type { FieldObjectType } from 'types/Fields';
import styles from './FormField.scss';
import { DateTimePicker } from '@mui/x-date-pickers';


type FieldPropsType = {
  error: boolean;
  helperText?: string;
  options?: any[];
  [ type: string ]: any;
}

const FormField: FC<FieldObjectType & { disabled?: boolean }> = (props): ReactElement => {
  const {
    field,
    bind,
    isError,
    errorMessage,
    disabled = false,
  } = props;
  const fields: { [ type: string ]: FC<FieldPropsType> } = {
    'email': ({ options, ...compProps }) => <TextField  {...compProps} {...field.htmlAttr}/>,
    'password': ({ options, ...compProps }) => <TextField {...compProps} {...field.htmlAttr}/>,
    'text': ({ options, ...compProps }) => <TextField {...compProps} {...field.htmlAttr}/>,
    'textarea': ({ options, ...compProps }) => <TextField multiline minRows={4} {...compProps} {...field.htmlAttr}/>,
    'autocomplete': ({ error, helperText, ...compProps }) => {
      const value = compProps.options?.find(item => item.value === compProps.value);
      return (
        <Autocomplete
          disablePortal
          renderInput={(textFieldsParams) => (
            <TextField
              {...textFieldsParams}
              label={field.label}
              error={error}
              helperText={helperText}
            />
          )}
          {...field.htmlAttr}
          {...compProps}
          value={value?.label || ''}
        />
      );
    },
    'autocompleteMultiple': ({ error, helperText, ...compProps }) => {
      const values = compProps.options
        ?.filter(item => compProps.value.includes(item.value))

      return (
        <Autocomplete
          disablePortal
          multiple
          renderInput={(textFieldsParams) => (
            <TextField
              {...textFieldsParams}
              label={field.label}
              error={error}
              helperText={helperText}
            />
          )}
          {...field.htmlAttr}
          {...compProps}
          value={values || []}
        />
      );
    },
    'date': ({ options, error, helperText, onChange, ...compProps }) => {
      return (
        <MobileDatePicker
          value={dayjs()}
          renderInput={(params) => <TextField
            fullWidth
            {...params}
            error={error}
            helperText={errorMessage}
          />}
          inputFormat="DD/MM/YYYY"
          onChange={onChange}
          {...compProps}
        />
      );
    },
    'time': ({ options, error, helperText, onChange, ...compProps }) => {
      return (
        <MobileTimePicker
          value={dayjs()}
          renderInput={(params) => <TextField
            fullWidth
            {...params}
            error={error}
            helperText={errorMessage}
          />}
          ampm={false}
          label="Час початку роботи"
          onChange={onChange}
          {...compProps}
        />
      );
    },
  };
  const FieldRef = useRef<FC<any>>(fields[ field.type ]);
  return (
    <div className={cx(styles.FormField)}>
      <FieldRef.current
        label={field.label}
        {...bind}
        error={isError}
        helperText={errorMessage}
        fullWidth
        options={field.options}
        disabled={disabled || bind.disabled}
      />
    </div>
  );
};

export default FormField;

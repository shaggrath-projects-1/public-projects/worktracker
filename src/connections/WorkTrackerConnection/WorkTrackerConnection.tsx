import React, { FC, ReactElement, ReactNode, useRef } from 'react';
import { ApolloClient, InMemoryCache, ApolloProvider, ApolloLink, HttpLink } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { RetryLink } from '@apollo/client/link/retry';
import config from 'config';
import Typography from '@mui/material/Typography';
import { useSnackbar } from 'notistack';
import useAuth from 'hooks/useAuth';

type DumpType = {
  [ key: string ]: any;
}

type WorkTrackerConnectionPropsType = {
  children: ReactNode,
}

const WorkTrackerConnection: FC<WorkTrackerConnectionPropsType> = (props): ReactElement => {
  const { children } = props;
  const { enqueueSnackbar } = useSnackbar();
  const auth = useAuth();
  const errorHandler = (props: DumpType) => {
    const {graphQLErrors, networkError} = props;
    if (graphQLErrors) {
      graphQLErrors.forEach((exception: any) => {
        const { message, locations, path, code, debugMessage, extensions } = exception;
        console.log('[GraphQL error]:');
        console.log(`Code: ${code}`);
        console.log(`Message: ${message}`);
        console.log(`Debug Message: ${debugMessage}`);
        console.log(`Location: ${locations}`);
        console.log(`Path: ${path}`);
        console.log(`Extensions: ${extensions ? JSON.stringify(extensions) : '-'}`);
        console.log('------------------------------------------');
      });
    }

    if (networkError?.statusCode === 401) {
      console.log(`Error: `, networkError);
      return;
    }

    enqueueSnackbar(<Typography sx={{ fontSize: 14 }}>Помилка у мережі, спробуйте піздніше.</Typography>, {
      variant: 'error',
      anchorOrigin: {
        horizontal: 'center',
        vertical: 'top',
      },
    });
  };
  
  const httpClient = new HttpLink({
    uri: config.graphql.workTracker.uri,
    headers: {
      Authorization: `Bearer ${auth.token}`,
    },
  })

  const clientRef = useRef(new ApolloClient({
    link: ApolloLink.from([
      new RetryLink({
        attempts: {
          max: 5,
          retryIf: async (error) => {
            if (error.statusCode != 401) {
              return false;
            }

            const token = await auth.signIn();

            httpClient.options.headers.Authorization = `Bearer ${token}`;
            return true;
          },
        }
      }),
      onError(errorHandler),
      httpClient,
    ]),
    cache: new InMemoryCache(),
  }));

  return (
    <ApolloProvider client={clientRef.current}>
      {children}
    </ApolloProvider>
  );
};

export default WorkTrackerConnection;

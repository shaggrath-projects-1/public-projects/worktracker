import React, { useRef, FC, ReactElement, ReactNode, useState } from 'react';
import { App, Credentials, User } from 'realm-web';
import { ApolloClient, InMemoryCache, ApolloProvider, ApolloLink, HttpLink } from '@apollo/client';
import { RetryLink } from '@apollo/client/link/retry';
import config from 'config';

type WeatherConnectionPropsType = {
  children: ReactNode;
};

const WeatherConnection: FC<WeatherConnectionPropsType> = (props): ReactElement => {
  const {
    children,
  } = props;
  const appRef = useRef<App>(new App(config.realm.weather.appId));
  const getToken = async (): Promise<string> => {
    const credentials = Credentials.apiKey(config.graphql.weather?.token || '');
    const user: User = await appRef.current.logIn(credentials);
    return user.accessToken || '';
  }
  const httpClient = new HttpLink({
    uri: config.graphql.weather.uri,
    headers: {
      Authorization: ``,
    },
  });
  const clientRef = useRef(new ApolloClient({
    link: ApolloLink.from([
      new RetryLink({
        attempts: {
          max: 5,
          retryIf: async (error: any) => {
            if (error.statusCode != 401) {
              return false;
            }
            const token = await getToken();
            httpClient.options.headers.Authorization = `Bearer ${token}`;
            return true;
          },
        }
      }),
      httpClient
    ]),
    cache: new InMemoryCache(),
  }));
  return (
    <ApolloProvider client={clientRef.current}>
      {children}
    </ApolloProvider>
  );
};

export default WeatherConnection;

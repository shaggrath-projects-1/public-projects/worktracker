import React, { useRef } from 'react';

type UseIntersectionType = {
  observer: IntersectionObserver;
};

const useIntersection = (cb: (entry:  IntersectionObserverEntry) => void, delayTime: number = 0): UseIntersectionType => {
  const delay = useRef<NodeJS.Timeout>();
  const callback: IntersectionObserverCallback = ([entry]) => {
    if (!entry) {
      return;
    }

    if (!entry.isIntersecting) {
      return;
    }

    clearTimeout(delay.current);
    delay.current = setTimeout(() => {
      cb(entry);
    }, delayTime);
  }
  const options: IntersectionObserverInit = {
    root: document,
    threshold: 1,
  };
  const observer = new IntersectionObserver(callback, options)

  return {
    observer
  }
}

export default useIntersection;

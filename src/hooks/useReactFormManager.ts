import { useContext, useState, useEffect } from 'react';
import { Validator } from 'livr';
import extraRules from 'livr-extra-rules';
import { ReactFormManagerContext } from 'context/ReactFormManager';
import { getFieldType } from 'utils/fields';
import type { UseFormManagerOptions } from 'types/Global';
import type {
  FieldsType,
  ExtendedFieldType,
  ValueType,
  FieldObjectType,
  BulkUpdateFieldsType,
  ValidatorDataType,
  FieldsObjectType,
  ValidationResult,
} from 'types/Fields';

type UseReactFormManagerType = {
  formFields: FieldObjectType[];
  formFieldsObject: FieldsObjectType;
  validateForm: () => ValidationResult;
  resetForm: () => void;
  bulkUpdateFields: BulkUpdateFieldsType;
}

const useReactFormManager = (fields: FieldsType, hookOptions: UseFormManagerOptions = {}): UseReactFormManagerType => {
  const reactFormManagerCTX = useContext(ReactFormManagerContext);
  const options = { ...reactFormManagerCTX.options, ...hookOptions };
  Validator.defaultAutoTrim(true);
  Validator.registerDefaultRules(extraRules);
  Validator.registerDefaultRules(reactFormManagerCTX.customValidationRules);
  const fieldsArray: ExtendedFieldType[] = Object.keys(fields).map(key => ({
    ...fields[ key ],
    name: key,
    originalType: fields[ key ].type,
    value: fields[ key ].defaultValue,
    onBeforeChange: () => true,
    onAfterChange: () => {
    },
  }));
  const formFields: FieldObjectType[] = fieldsArray.map(field => {
    const [ value, setValue ] = useState<ValueType>(field.defaultValue || '');
    const [ type, setType ] = useState<string>(getFieldType(field, fieldsArray));
    const [ isError, setIsError ] = useState<boolean>(false);
    const [ errorMessage, setErrorMessage ] = useState<string>('');
    const onChangeHandler = (...props: any) => {
      const valueSelector =
        (reactFormManagerCTX.fieldValueSelector || {})[ field.originalType ]
        || (reactFormManagerCTX.fieldValueSelector || {})[ 'default' ];
      const newValue = valueSelector(...props);

      if (!field.onBeforeChange(newValue, bulkUpdateFields)) {
        return;
      }

      setValue(newValue);
      field.onAfterChange(newValue, bulkUpdateFields);
    };
    const fieldObject: FieldObjectType = {
      field,
      name: field.name,
      value,
      isError,
      errorMessage,
      setValue,
      setType,
      setIsError,
      setErrorMessage,
      bind: {
        name: field.name,
        id: field.name,
        type,
        value,
        onChange: onChangeHandler,
        ...field.htmlAttr,
      },
    };

    useEffect(() => {
      bulkUpdateFieldsType();

      if (options.isValidateOnChange) {
        validateForm();
      } else {
        setIsError(false);
        setErrorMessage('');
      }

    }, [ value ]);

    return fieldObject;
  });
  const formFieldsObject = formFields.reduce<FieldsObjectType>((acc, field) => {
    acc[ field.name ] = {
      value: field.value,
      setError: (message) => {
        field.setIsError(!!message.length);
        field.setErrorMessage(message);
      },
      setValue: field.setValue,
    };
    return acc;
  }, {});
  const bulkUpdateFieldsType = (): void => {
    formFields.forEach(formField => {
      formField.setType(getFieldType(formField.field, fieldsArray));
    });
  };
  const bulkUpdateFields: BulkUpdateFieldsType = (fields) => {
    formFields.forEach(field => {
      if (typeof fields[ field.name ] !== 'undefined') {
        field.setValue(fields[ field.name ]);
      }
    });
  };
  const resetForm = () => {
    const emptyValues = formFields.reduce<{ [ fieldName: string ]: string }>((acc, item) => ({ ...acc, [ item.name ]: '' }), {});
    bulkUpdateFields(emptyValues);
  };
  const validateForm = (): ValidationResult => {
    const validatorData = formFields.reduce<ValidatorDataType>((acc, field) => {
      acc.rules[ field.name ] = field.field.validationRules;
      acc.data[ field.name ] = field.value;
      field.setErrorMessage('');
      field.setIsError(false);
      return acc;
    }, {
      rules: {},
      data: {},
    });
    const validator = new Validator(validatorData.rules);
    const validData = validator.validate(validatorData.data);
    const errors = validator.getErrors();

    if (!errors) {
      return {
        ...validatorData.data,
        ...validData,
      };
    }

    formFields.forEach(field => {
      if (!errors[ field.name ]) {
        return;
      }
      field.setIsError(true);
      field.setErrorMessage(
        (reactFormManagerCTX.errorMessages || {})[ errors[ field.name ] ]
        || (reactFormManagerCTX.errorMessages || {}).default,
      );
    });

    return;
  };

  return {
    formFields,
    formFieldsObject,
    validateForm,
    resetForm,
    bulkUpdateFields,
  };
};

export default useReactFormManager;

import React, { useEffect, useRef, useState } from 'react';
import type { FC, ReactElement } from 'react';
import cx from 'classnames';
import dayjs from 'dayjs';
import { useQuery } from '@apollo/client';
import { LineChart, Line, XAxis, YAxis, Legend, Tooltip } from 'recharts';
import Typography from '@mui/material/Typography';
import Caption from 'components/Caption';
import Box from 'components/Box';
import Loader from 'components/Loader';
import { Datum } from 'types/gql-weather-types';
import { weatherQuery } from './queries';
import useIntersection from 'hooks/intersection';
import styles from './Weather.scss';

const Weather: FC = (): ReactElement => {
  const { observer } = useIntersection((entry: IntersectionObserverEntry) => {
    alert(entry.target.id);
  }, 300);
  // const boxRef = useRef<HTMLDivElement>(null);
  // const [ chartWidth, setChartWidth ] = useState<number>(0);
  // const { data, loading, error } = useQuery<{ data: Datum[] }>(weatherQuery);
  //
  // useEffect(() => {
  //   if (!boxRef.current) {
  //     return;
  //   }
  //   const computedStyle = getComputedStyle(boxRef.current);
  //   const elementClientWidth = boxRef.current.clientWidth;
  //   const elementWidth = elementClientWidth - (parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight));
  //   setChartWidth(elementWidth);
  // }, [ loading ]);
  //
  // if (loading || error) {
  //   return (<Loader/>);
  // }
  //
  // const chartData = (data?.data || []).map((item) => item).reverse();
  //
  // if (!chartData.at(-1)) {
  //   return <span>no data</span>
  // }

  return (
    <div className={cx(styles.Weather)}>
      {new Array(100).fill('').map((item, index) => (
        <Item
          key={`${index}-item`}
          index={index}
          observer={observer}
        />

      ))}
      {/*<Box viewType="dark" className={cx(styles.Weather__itemsWrapper)}>*/}
      {/*  <article className={cx(styles.Weather__item)}>*/}
      {/*    <Typography>Зовнішня температура:</Typography>*/}
      {/*    <Caption>{`${chartData.at(-1)?.outsideTemperature} °C`}</Caption>*/}
      {/*  </article>*/}
      {/*  <article className={cx(styles.Weather__item)}>*/}
      {/*    <Typography>Зовнішня вологість:</Typography>*/}
      {/*    <Caption>{`${chartData.at(-1)?.outsideHumidity}%`}</Caption>*/}
      {/*  </article>*/}
      {/*  <article className={cx(styles.Weather__item)}>*/}
      {/*    <Typography>Висота:</Typography>*/}
      {/*    <Caption>{`${chartData.at(-1)?.altitude} m`}</Caption>*/}
      {/*  </article>*/}
      {/*  <article className={cx(styles.Weather__item)}>*/}
      {/*    <Typography>Внутрішня температура:</Typography>*/}
      {/*    <Caption>{`${chartData.at(-1)?.insideTemperature} °C`}</Caption>*/}
      {/*  </article>*/}
      {/*  <article className={cx(styles.Weather__item)}>*/}
      {/*    <Typography>Внутрішня вологість:</Typography>*/}
      {/*    <Caption>{`${chartData.at(-1)?.insideHumidity}%`}</Caption>*/}
      {/*  </article>*/}
      {/*</Box>*/}
      {/*<Box className={cx(styles.Weather__chart)} ref={boxRef}>*/}
      {/*  <Typography variant="h3" className={cx(styles.Weather__header)}>*/}
      {/*    Показники параметрів за добу*/}
      {/*  </Typography>*/}
      {/*  <LineChart width={chartWidth} height={600} data={chartData}>*/}
      {/*    <Legend*/}
      {/*      formatter={(value) => {*/}
      {/*        switch (value) {*/}
      {/*          case 'outsideTemperature':*/}
      {/*            return 'Зовнішня температура';*/}
      {/*          case 'insideTemperature':*/}
      {/*            return 'Внутрішня температура';*/}
      {/*          case 'outsideGas':*/}
      {/*            return 'Спротив повітря';*/}
      {/*          case 'outsideHumidity':*/}
      {/*            return 'Зовнішня вологість';*/}
      {/*          default:*/}
      {/*            return value;*/}
      {/*        }*/}
      {/*      }}*/}
      {/*    />*/}
      {/*    <Tooltip*/}
      {/*      contentStyle={{*/}
      {/*        backgroundColor: '#0a1929',*/}
      {/*        borderRadius: '10px',*/}
      {/*        border: '1px solid #132f4c',*/}
      {/*      }}*/}
      {/*      labelFormatter={(label, payload) => dayjs(label).format('DD, MMM YYYY HH:mm')}*/}
      {/*      formatter={(value, name, item, index, payload) => {*/}
      {/*        switch (name) {*/}
      {/*          case 'outsideTemperature':*/}
      {/*            return [ `${value} °C`, 'Зовнішня температура' ];*/}
      {/*          case 'insideTemperature':*/}
      {/*            return [ `${value} °C`, 'Внутрішня температура' ];*/}
      {/*          case 'outsideGas':*/}
      {/*            return [ `${value} kΩ`, 'Спротив повітря' ];*/}
      {/*          case 'outsideHumidity':*/}
      {/*            return [ `${value}%`, 'Зовнішня вологість' ];*/}
      {/*          default:*/}
      {/*            return [ value, name ];*/}
      {/*        }*/}
      {/*      }}*/}
      {/*    />*/}
      {/*    <Line type="monotone" dataKey="outsideTemperature" stroke="#0e78f8" dot={{ r: 1 }}/>*/}
      {/*    <Line type="monotone" dataKey="insideTemperature" stroke="#dc7e10" dot={{ r: 1 }}/>*/}
      {/*    <Line type="monotone" dataKey="outsideGas" stroke="#3ba55d" dot={{ r: 1 }}/>*/}
      {/*    <Line type="monotone" dataKey="outsideHumidity" stroke="#f1c40f" dot={{ r: 1 }}/>*/}
      {/*    <XAxis*/}
      {/*      dataKey="time"*/}
      {/*      tickFormatter={(value) => dayjs(value).format('HH:mm')}*/}
      {/*    />*/}
      {/*    <YAxis/>*/}
      {/*  </LineChart>*/}
      {/*</Box>*/}
    </div>

  );
};

const Item: FC<{ index: number; observer: IntersectionObserver  }> = (props) => {
  const {
    observer,
    index
  } = props;
  const itemRef = useRef<any>();

  useEffect(() => {
    observer.observe(itemRef.current);

    return () => {
      observer.unobserve(itemRef.current);
    }
  }, []);
  return <li
    id={`${index}`}
    ref={itemRef}
    key={`${index}-item`}
    style={{height: 100, width: 200,}}
  >
    {`Scrolled item ${index}`}
  </li>
}

export default Weather;

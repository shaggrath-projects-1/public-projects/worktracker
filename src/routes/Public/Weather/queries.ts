import { gql } from '@apollo/client';

export const weatherQuery = gql`
    query {
        data(query: {}, limit: 144, sortBy: TIME_DESC) {
            altitude
            insideTemperature
            insideHumidity
            insidePressure
            outsideTemperature
            outsideHumidity
            outsidePressure
            outsideGas
            time
        }
    }
`;

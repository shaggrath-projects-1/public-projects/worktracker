import React, { FC, ReactElement, useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import WorkTrackerConnection from 'connections/WorkTrackerConnection';
import useAuth from 'hooks/useAuth';

const Private: FC = (): ReactElement => {
  const { isSignIn, signIn } = useAuth();
  useEffect(() => {
    if (!isSignIn) {
      signIn();
    }
  }, [ isSignIn ]);

  return (
    <WorkTrackerConnection>
      <Outlet/>
    </WorkTrackerConnection>
  );
};

export default Private;

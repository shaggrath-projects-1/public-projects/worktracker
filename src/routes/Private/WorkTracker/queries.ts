import { gql } from '@apollo/client';
export const queryUsersData = gql`
    query($fromDate: DateTime, $toDate: DateTime) {
        users {
            _id
            name
        }
        logs( query: { date_gte: $fromDate, date_lte: $toDate }, sortBy: DATE_ASC) {
            _id
            date
            desc
            timeEnd
            timeStart
            user {
                name
                _id
            }
        }
    }
`;
export const queryUsersLogs = gql`
    query($fromDate: DateTime, $toDate: DateTime) {
        logs( query: { date_gte: $fromDate, date_lte: $toDate }, sortBy: DATE_ASC) {
            _id
            date
            desc
            timeEnd
            timeStart
            user {
                name
            }
        }
    }
`;

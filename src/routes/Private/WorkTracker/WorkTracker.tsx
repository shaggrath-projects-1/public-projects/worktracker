import React, { FC, FormEvent, Fragment, ReactElement, useState } from 'react';
import dayjs from 'dayjs';
import cx from 'classnames';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import CircularProgress from '@mui/material/CircularProgress';
import Collapse from '@mui/material/Collapse';
import Button from '@mui/material/Button';
import { useQuery } from '@apollo/client';
import { User, Log, LogQueryInput, Scalars } from 'types/gql-timeTracker-types';
import DrawerLayout from "layouts/DrawerLayout";
import FormField from 'components/FormField';
import AddLogForm from 'components/Forms/AddLogForm';
import Loader from 'components/Loader';
import Box from 'components/Box';
import useReactFormManager from 'hooks/useReactFormManager';
import { decimalAdjust } from 'utils/decimalAdjast';
import { queryUsersData } from './queries';
import { filterLogsRequestFields } from './fields';
import styles from './WorkTracker.scss';

type DataType = {
  logs: Log[],
  users: User[]
};

const WorkTracker: FC = (): ReactElement => {
  const { formFields, validateForm } = useReactFormManager(filterLogsRequestFields);
  const [ openedUsers, setOpenedUser ] = useState<string[]>([]);
  const [ startDate, setStartDate ] = useState(dayjs().startOf('months').toISOString());
  const [ endDate, setEndDate ] = useState(dayjs().endOf('months').toISOString());
  const { data, loading, error, refetch } = useQuery<DataType>(queryUsersData, {
    errorPolicy: 'ignore',
    variables: {
      fromDate: startDate,
      toDate: endDate,
    },
  });
  const toggleUser = (userId: string) => () => {
    setOpenedUser((prevState) => {
      if (prevState.includes(userId)) {
        return prevState.filter(item => item !== userId);
      }

      return [ ...prevState, userId ];
    });
  };

  const onSubmitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const validData = validateForm();
    if (!validData) {
      return;
    }
    const start = dayjs(validData.dateStart).startOf('day');
    const end = dayjs(validData.dateEnd).endOf('day');
    setStartDate(start.toISOString());
    setEndDate(end.toISOString());
  };

  if (loading) {
    return (
      <Loader/>
    )
  }

  const userOptions = !!data
    ? data.users.map(user => ({ value: user._id, label: user?.name || '' }))
    : []

  return (
    <DrawerLayout
      asideNode={(
        <Box className={cx(styles.WorkTracker__addLogForm)} viewType="dark">
          <AddLogForm userOptions={userOptions} onSubmitSuccess={refetch}/>
        </Box>
      )}
      mainNode={(
        <Box>
          <form
            id="get-report"
            onSubmit={onSubmitHandler}
            className={cx(styles.WorkTracker__getReportForm)}
          >
            {formFields.map(formField => (
              <FormField
                key={formField.name}
                {...formField}
              />
            ))}
            <Button
              disabled={loading}
              size="large"
              variant="contained"
              type="submit"
            >
              Завантажити
            </Button>
          </form>
          {!error && data?.users.map(user => {
            const logs = data?.logs.filter((log: any) => log.user._id === user._id);
            const time = logs.reduce((acc: number, item: any) => {
              const date1 = dayjs(item.timeEnd);
              const diff = date1.diff(item.timeStart, 'minutes');
              return decimalAdjust('ceil', acc + (diff / 60), -1);
            }, 0);

            return (
              <article key={user._id} className={cx(styles.User)}>
                <div className={cx(styles.User__title)}>
                  <Typography className={cx(styles.User__name)}>
                    {user.name}
                  </Typography>
                  <Typography className={cx(styles.User__hours)}>
                    {`${time} годин`}
                  </Typography>
                </div>
                <Collapse in={openedUsers.includes(user._id)}>
                  <List>
                    {logs.map((log: any, index: number) => {
                      const date1 = dayjs(log.timeEnd);
                      const diff = decimalAdjust('ceil', date1.diff(log.timeStart, 'minutes') / 60, -1);

                      return (
                        <Fragment key={log._id}>
                          {index === 0 && (<Divider component="li"/>)}
                          <ListItem>
                            <ListItemText
                              primary={`${log.desc} (${diff} години)`}
                              secondary={`${dayjs(log.date).format('DD/MM/YYYY')} з ${dayjs(log.timeStart).format('HH:mm')} по ${dayjs(log.timeEnd).format('HH:mm')}`}
                            />
                          </ListItem>
                          <Divider component="li"/>
                        </Fragment>
                      );
                    })}
                  </List>
                </Collapse>
                <Button onClick={toggleUser(user._id)} className={cx(styles.User__btn)}>
                  {openedUsers.includes(user._id) ? 'Приховати деталі' : 'Показати деталі'}
                </Button>
              </article>
            );
          })}
        </Box>
      )}
    />
  );
};

export default WorkTracker;

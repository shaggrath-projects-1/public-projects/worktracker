import React, { FC, FormEvent, Fragment, ReactElement, useState } from 'react';
import dayjs, { Dayjs } from 'dayjs';
import cx from 'classnames';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import Button from '@mui/material/Button';
import { useQuery } from '@apollo/client';
import { User, Shift } from 'types/gql-timeTracker-types';
import Caption from 'components/Caption';
import FormField from 'components/FormField';
import Loader from 'components/Loader';
import Box from 'components/Box';
import useReactFormManager from 'hooks/useReactFormManager';
import { queryUsersData } from './queries';
import { filterLogsRequestFields } from './fields';
import styles from './Shifts.scss';

type DataType = {
  shifts: Shift[],
  users: User[]
};

const Shifts: FC = (): ReactElement => {
  const { formFields, validateForm } = useReactFormManager(filterLogsRequestFields);
  const [ openedUsers, setOpenedUser ] = useState<string[]>([]);
  const [ startDate, setStartDate ] = useState(dayjs().startOf('weeks').toISOString());
  const [ endDate, setEndDate ] = useState(dayjs().endOf('weeks').toISOString());
  const { data, loading, error } = useQuery<DataType>(queryUsersData, {
    errorPolicy: 'ignore',
    variables: {
      fromDate: startDate,
      toDate: endDate,
    },
  });
  const toggleUser = (userId: string) => () => {
    setOpenedUser((prevState) => {
      if (prevState.includes(userId)) {
        return prevState.filter(item => item !== userId);
      }

      return [ ...prevState, userId ];
    });
  };

  const onSubmitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const validData = validateForm();
    if (!validData) {
      return;
    }
    const start = dayjs(validData.dateStart).startOf('day');
    const end = dayjs(validData.dateEnd).endOf('day');
    setStartDate(start.toISOString());
    setEndDate(end.toISOString());
  };

  if (loading) {
    return (
      <Loader/>
    );
  }

  return (
    <Box>
      <form
        id="get-report"
        onSubmit={onSubmitHandler}
        className={cx(styles.Shifts__getReportForm)}
      >
        {formFields.map(formField => (
          <FormField
            key={formField.name}
            {...formField}
          />
        ))}
        <Button
          disabled={loading}
          size="large"
          variant="contained"
          type="submit"
        >
          Завантажити
        </Button>
      </form>
      {!error && data?.users.map(user => {
        const userShifts = (data?.shifts || [])
          .filter((shift) => shift.uuid === user.uuid)
          .reduce<Shift[]>((acc, shift) => {
            const format = 'DD-MM-YYYY';
            const isExistNow = acc.some(item => dayjs(item.date).format(format) === dayjs(shift.date).format(format));

            if (!isExistNow) {
              acc.push(shift);
            }

            return acc;
          }, []);

        return (
          <article key={user._id} className={cx(styles.User)}>
            <div className={cx(styles.User__title)}>
              <Typography className={cx(styles.User__name)}>
                {user.name}
              </Typography>
              <Caption>
                {`${userShifts.length} змін`}
              </Caption>
            </div>
            <Collapse in={openedUsers.includes(user._id)}>
              <List>
                {userShifts.map((shift, index: number) => {
                  return (
                    <Fragment key={shift._id}>
                      {index === 0 && (<Divider component="li"/>)}
                      <ListItem>
                        <ListItemText
                          primary={`Зміна ${index + 1}`}
                          secondary={`${dayjs(shift.date).format('DD, MMM YYYY HH:mm')}`}
                        />
                      </ListItem>
                      <Divider component="li"/>
                    </Fragment>
                  );
                })}
              </List>
            </Collapse>
            <Button onClick={toggleUser(user._id)} className={cx(styles.User__btn)}>
              {openedUsers.includes(user._id) ? 'Приховати деталі' : 'Показати деталі'}
            </Button>
          </article>
        );
      })}
    </Box>
  );
};

export default Shifts;

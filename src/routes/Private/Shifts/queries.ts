import { gql } from '@apollo/client';
export const queryUsersData = gql`
    query($fromDate: DateTime, $toDate: DateTime) {
        users {
            _id
            name
            uuid
        }
        shifts(query: { date_gte: $fromDate, date_lte: $toDate }, sortBy: DATE_ASC) {
            date
            uuid
        }
    }
`;

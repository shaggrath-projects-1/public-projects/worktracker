import type { FieldsType } from 'types/Fields';
import dayjs from 'dayjs';

export const filterLogsRequestFields: FieldsType = {
  dateStart: {
    type: 'date',
    label: 'З дати',
    validationRules: [ 'required' ],
    defaultValue: dayjs().startOf('weeks').valueOf(),
  },
  dateEnd: {
    type: 'date',
    label: 'По дату',
    validationRules: [ 'required' ],
    defaultValue: dayjs().endOf('weeks').valueOf(),
  },
};

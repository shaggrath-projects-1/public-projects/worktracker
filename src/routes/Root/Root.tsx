import type { FC, ReactElement } from 'react';
import React from 'react';
import { Route, Routes } from 'react-router-dom';
import MainLayout from 'layouts/MainLayout';
import Private from 'routes/Private';
import WeatherConnection from 'connections/WeatherConnection';
import WorkTracker from 'routes/Private/WorkTracker';
import Shifts from 'routes/Private/Shifts';
import Weather from 'routes/Public/Weather/Weather';


const Root: FC = (): ReactElement => {
  return (
    <MainLayout>
      <Routes>
        <Route index element={(
          <WeatherConnection>
            <Weather/>
          </WeatherConnection>
        )}/>
        <Route path={'private'} element={<Private/>}>
          <Route path={'work-tracker'} element={<WorkTracker/>}/>
          <Route path={'shifts'} element={<Shifts/>}/>
        </Route>
      </Routes>
    </MainLayout>
  );
};

export default Root;

type GraphQLConnectionConfig = {
  uri: string;
  token?: string;
};
type RealmType = {
  appId: string;
}
type ConfigType = {
  realm: {
    workTracker: RealmType,
    weather: RealmType,
  },
  graphql: {
    workTracker: GraphQLConnectionConfig;
    weather: GraphQLConnectionConfig;
  }
}

const config: ConfigType = {
  realm: {
    workTracker: {
      appId: 'timetracker-cpoub',
    },
    weather: {
      appId: 'weather-jgzab',
    }
  },
  graphql: {
    workTracker: {
      uri: 'https://eu-west-1.aws.realm.mongodb.com/api/client/v2.0/app/timetracker-cpoub/graphql',
    },
    weather: {
      uri: 'https://eu-west-1.aws.realm.mongodb.com/api/client/v2.0/app/weather-jgzab/graphql',
      token: 'Bq94f9n2ChDSpIb9m68mHoxMjC1QuP5kdguAcWoB0ABRiuEl0AHIRb99sHrlfgDg',
    },
  },
};

export default config;

/** @type {import('graphql-config').IGraphQLConfig } */
/** @type {string[]} */
const plugins = [ 'typescript', 'typescript-operations', 'typescript-react-query', 'typescript-react-apollo' ];
module.exports = {
  projects: {
    timeTracker: {
      schema: [ './timeTracker.graphql' ],
      extensions: {
        codegen: {
          generates: {
            './src/types/gql-timeTracker-types.d.ts': {
              plugins,
            },
          },
        },
      },
    },
    weather: {
      schema: [ './weather.graphql' ],
      extensions: {
        codegen: {
          generates: {
            './src/types/gql-weather-types.d.ts': {
              plugins,
            },
          },
        },
      },
    },
  },
};
